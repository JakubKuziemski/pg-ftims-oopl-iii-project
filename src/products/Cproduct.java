package products;

/**
 * Created by Kuba on 16.01.2016.
 */
public class Cproduct
{
    private String mName;
    private Double mPrice;

    protected Cproduct(String mName , Double mPrice)
    {
        this. mName = mName;
        this. mPrice = mPrice;
    }

    public Double getPrice()
    {
        return mPrice;
    }

    public String getName()
    {
        return mName;
    }

    public void setPrice(Double mPrice)
    {
        this.mPrice = mPrice;
    }
}
