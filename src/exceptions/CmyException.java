package exceptions;

/**
 * Created by Kuba on 18.01.2016.
 */
public class CmyException extends Exception
{
    public CmyException(String message)
    {
        super(message);
    }
}
