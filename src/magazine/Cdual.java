package magazine;

import products.Cproduct;

/**
 * Created by Kuba on 16.01.2016.
 */
public class Cdual
{
    private Cproduct mKey;//nazwa , cena
    private int mValue;//ilosc
    public Cdual(Cproduct mKey , int mValue)
    {
        this.mKey = mKey;
        this.mValue = mValue;
    }

    public Cproduct getmKey()
    {
        return mKey;
    }

    public int value()
    {
        return mValue;
    }

    public void setValue(int mValue)
    {
        this.mValue = mValue;
    }
    public void value(int v)
    {
        this.mValue+=v;
    }
    public boolean compare(Cproduct C)
    {
        return mKey.getClass() == C.getClass();
    }
    @Override
    public String toString()
    {
        return mKey.getName() + " " + mKey.getPrice() + " " + mValue;
    }
}
