package wholesales;

import address.Caddress;
import address.Ccustomer;
import products.Cproduct;

/**
 * Created by Kuba on 17.01.2016.
 */
public class CwholesaleOfHydraulicArticles extends Cwholesale
{
    public static String mGetWholesaleName = "Hurtownia artykułów hydraulicznych";
    public CwholesaleOfHydraulicArticles(Ccustomer mOwner, Caddress mAddress , String nameOfWholesale, Cproduct... args)
    {
        super(mOwner, mAddress, args);
    }
    public String getWholesaleName()
    {
        return mGetWholesaleName;
    }
}
