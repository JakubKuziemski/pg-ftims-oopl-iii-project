package wholesales;

import address.Caddress;
import address.Ccustomer;
import exceptions.CmyException;
import interfaces.IMagazineStatus;
import interfaces.IOwnerData;
import magazine.Cdual;
import products.Cproduct;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Kuba on 16.01.2016.
 */
public class Cwholesale extends CnetworkOfWholesales implements IMagazineStatus, IOwnerData
{
    //client number
    private Map<Integer, Double> mProductsBoughtByCustomer;
    private Ccustomer mCustomer;
    private List<Cdual> mListOfProducts;


    public Cwholesale(Ccustomer mOwner, Caddress mAddress , Cproduct... args)
    {
        super(mOwner, mAddress);

        mProductsBoughtByCustomer = new TreeMap<>();
        mListOfProducts = new ArrayList<>();
        mCustomer = null;

        for(int i = 0 ; i < args.length ; i++)
        {
            mListOfProducts.add(new Cdual(args[i], 100));
        }
    }

    public void addCustomer(Ccustomer customer)
    {
        mCustomer = customer;

        if (!mProductsBoughtByCustomer.containsKey(customer.getmNumber()))
        {
            mProductsBoughtByCustomer.put(customer.getmNumber(), 0.0);
        }
    }

    public void removeCustomer()
    {
        mCustomer = null;
    }

    public int getIndexOfCustomer()
    {
        return mCustomer.getmNumber()-1;
    }

    public String getProduct(Cproduct product , int howMany )throws CmyException
    {
        boolean productFound = false;
        int i;
        for(i = 0 ; i < mListOfProducts.size() ; i++)
        {
           if(mListOfProducts.get(i).compare(product))
            {
                productFound = true;
                break;
            }
        }
        if(!productFound)
        {
            throw new CmyException("Przepraszamy, nie mamy tego produktu");
        }

        if(mListOfProducts.get(i).value() >= howMany)
        {

            try
            {
                mListOfProducts.get(i).value(-howMany);
                double cash = mProductsBoughtByCustomer.remove(mCustomer.getmNumber());
                cash+= howMany * product.getPrice();
                mProductsBoughtByCustomer.put(mCustomer.getmNumber() , cash);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return "Proszę odebrać produkt/y";
        }
        throw new CmyException("Nie mamy wystarczającej ilosci produktu");
    }

    @Override
    public void printStatus()
    {
        for(int i = 0 ; i < mListOfProducts.size() ; i++)
        {
            System.out.println(mListOfProducts.get(i));
        }
    }
    public void printMapStatus()
    {
        for (Integer i:mProductsBoughtByCustomer.keySet())
        {

            System.out.println("Numer klienta: " +  i +  "\ndo zapłaty: " +  mProductsBoughtByCustomer.get(i));
            System.out.println("");
        }
    }

    @Override
    public void printOwnerData()
    {
        Ccustomer owner = super.getmOwner();
        Caddress addressOfOwner = super.getmAddress();
        System.out.println(owner.getnName()+" " + owner.getnSurname() + " " + owner.getnAddressOfCustomer());
    }
}
