package wholesales;

import address.Caddress;
import address.Ccustomer;

/**
 * Created by Kuba on 16.01.2016.
 */
public class CnetworkOfWholesales
{
    private Ccustomer mOwner;
    private Caddress mAddress;
    protected CnetworkOfWholesales(Ccustomer mOwner , Caddress mAddress)
    {
        this. mOwner = mOwner;
        this. mAddress = mAddress;
    }

    public Caddress getmAddress()
    {
        return mAddress;
    }

    public Ccustomer getmOwner()
    {
        return mOwner;
    }
}
