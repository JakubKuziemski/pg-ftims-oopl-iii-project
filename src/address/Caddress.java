package address;

/**
 * Created by Kuba on 16.01.2016.
 */
public class Caddress
{
    private String mPostalCode;
    private String mPlace;
    private String mStreet;
    private String mCountry;
    private String mNumberOfBuilding;
    private String mNumberOfLocal;

    public Caddress(String mPostalCode , String mPlace , String mStreet , String mCountry , String mNumberOfBuilding , String mNumberOfLocal)
    {
        this. mPostalCode = mPostalCode;
        this. mPlace = mPlace;
        this. mStreet = mStreet;
        this. mCountry = mCountry;
        this. mNumberOfBuilding = mNumberOfBuilding;
        this. mNumberOfLocal = mNumberOfLocal;
    }
    public Caddress(String mPostalCode , String mPlace , String mStreet , String mCountry , String mNumberOfBuilding)
    {
        this. mPostalCode = mPostalCode;
        this. mPlace = mPlace;
        this. mStreet = mStreet;
        this. mCountry = mCountry;
        this. mNumberOfBuilding = mNumberOfBuilding;
        this. mNumberOfLocal = "";
    }

    public String getmPostalCode()
    {
        return mPostalCode;
    }

    public String getmPlace()
    {
        return mPlace;
    }

    public String getmStreet()
    {
        return mStreet;
    }

    public String getmCountry()
    {
        return mCountry;
    }

    public String getmNumberOfBuilding()
    {
        return mNumberOfBuilding;
    }

    public String getmNumberOfLocal()
    {
        return mNumberOfLocal;
    }
    @Override
    public String toString()
    {
        return mStreet + " " + mNumberOfBuilding + " " + mNumberOfLocal + " " + mPostalCode + " " + mPlace + " " + mCountry;
    }
}
