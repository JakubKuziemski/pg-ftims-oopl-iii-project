package address;

/**
 * Created by Kuba on 16.01.2016.
 */
public class Ccustomer
{
    private String nName;
    private String nSurname;
    private Caddress nAddressOfCustomer;
    private Integer mNumber;

    public Ccustomer(String nName , String nSurname , Caddress nAddressOfCustomer , Integer mNumber)
    {
        this.nName = nName;
        this.nSurname = nSurname;
        this.mNumber = mNumber;
        this.nAddressOfCustomer = nAddressOfCustomer;
    }

    public String getnName()
    {
        return nName;
    }

    public String getnSurname()
    {
        return nSurname;
    }

    public Integer getmNumber()
    {
            return mNumber;
    }

    public Caddress getnAddressOfCustomer()
    {
        return nAddressOfCustomer;
    }
    @Override
    public String toString()
    {
        return nName + " " + nSurname + " " + nAddressOfCustomer.toString();
    }
}
