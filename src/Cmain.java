import address.Caddress;
import address.Ccustomer;
import exceptions.CmyException;
import interfaces.IMagazineStatus;
import interfaces.IOwnerData;
import products.*;
import wholesales.Cwholesale;
import wholesales.CwholesaleOfBuildingMaterials;
import wholesales.CwholesaleOfHydraulicArticles;
import wholesales.CwholesaleOfceramics;

import java.util.Scanner;

/**
 * Created by Kuba on 16.01.2016.
 */

public class Cmain
{
    public static void printStatus(IMagazineStatus magazine)
    {
        magazine.printStatus();
    }
    public static void getStoreOwner(IOwnerData magazine)
    {
        magazine.printOwnerData();
    }

    public static void main(String[] args)
    {
        Caddress[] addressesOfCustomers = new Caddress[5];
        addressesOfCustomers[0] = new Caddress("86-170" , "Nowe" , "Wiślana" , "Polska" , "7" , "1");
        addressesOfCustomers[1] = new Caddress("80-300" , "Grudziądz" , "Wiślana" , "Polska" , "10" , "2");
        addressesOfCustomers[2] = new Caddress("80-300" , "Grudziądz" , "Chełmińska" , "Polska" , "80" , "32");
        addressesOfCustomers[3] = new Caddress("81-500" , "Pieniążkowo" , "Nowa" , "Polska" , "70" , "1");
        addressesOfCustomers[4] = new Caddress("10-340" , "Bochlin" , "Wyspiańskiego" , "Polska" , "40" , "7");

        Ccustomer[] customers = new Ccustomer[5];
        customers[0] = new Ccustomer("Jacek","Nowak",addressesOfCustomers[0],1);
        customers[1] = new Ccustomer("Marek","Nowak",addressesOfCustomers[1],2);
        customers[2] = new Ccustomer("Tomek","Nowak",addressesOfCustomers[2],3);
        customers[3] = new Ccustomer("Piotr","Nowak",addressesOfCustomers[3],4);
        customers[4] = new Ccustomer("Wojtek","Nowak",addressesOfCustomers[4],5);

        Caddress[] addressesOfOwners = new Caddress[3];
        addressesOfOwners[0] = new Caddress("84-120" , "Władysławowo" , "1000 - lecia P.P" , "8" , "4");
        addressesOfOwners[1] = new Caddress("82-156" , "Chojnice" , "Wyspainskiego" , "4" , "7");
        addressesOfOwners[2] = new Caddress("84-343" , "Gdańsk" , "Wyspiańskiego" , "7" , "207");

        Ccustomer[] owners = new Ccustomer[3];
        owners[0] = new Ccustomer("Wojtek" , "Stechura",addressesOfOwners[0],1);
        owners[1] = new Ccustomer("Michał" , "Błociński",addressesOfOwners[1],2);
        owners[2] = new Ccustomer("Jakub" , "Kuziemski",addressesOfOwners[2],3);

        Cproduct ceramics1 = new Cceramics("Płytki",30.0);
        Cproduct glass1 = new Cglass("Szyba",70.0);
        Cproduct hydraulicArticles1 = new ChydraulicArticles("Kran" , 50.0);
        Cproduct washbasin = new Cwashbasin("umywalka" , 150.5);
        Cproduct buildingMaterials = new CbuildingMaterials("Pustaki" , 15.5);
        Cproduct looseMaterials = new ClooseMaterials("Żwir" , 14.99);

        Caddress[] addressesOfWholesales = new Caddress[3];
        addressesOfWholesales[0] = new Caddress("87-230" , "Warszawa" , "Grudziadzka" , "4" , "1");
        addressesOfWholesales[1] = new Caddress("52-156" , "Gdynia" , "Hallera" , "5" , "6");
        addressesOfWholesales[2] = new Caddress("26-373" , "Gdańsk" , "Wyspiańskiego" , "78" , "20");

        Cwholesale wholesale1 = new CwholesaleOfBuildingMaterials(owners[0],addressesOfWholesales[0], CwholesaleOfBuildingMaterials.mGetWholesaleName, looseMaterials , buildingMaterials);
        Cwholesale wholesale2 = new CwholesaleOfceramics(owners[1] , addressesOfWholesales[1] , CwholesaleOfceramics.mGetWholesaleName , ceramics1 , glass1 );
        Cwholesale wholesale3 = new CwholesaleOfHydraulicArticles(owners[2] , addressesOfWholesales[2] , CwholesaleOfHydraulicArticles.mGetWholesaleName , hydraulicArticles1 , washbasin);

        Scanner sc = new Scanner(System.in);
        int number;
        boolean exit = false;
        Ccustomer temporaryCustomer = customers[0];
        Cwholesale temporaryWholesale = wholesale1;
        Cproduct temporaryProduct = ceramics1;
        while(true)
        {
            System.out.println("Wybierz klienta");
            for(int i = 0 ; i < 5 ; i++)
            {
                System.out.println((i+1)+ " " + customers[i]);
            }

            number = sc.nextInt();
            switch (number)
            {
                case 1:
                    temporaryCustomer = customers[0];
                    break;
                case 2:
                    temporaryCustomer = customers[1];
                    break;
                case 3:
                    temporaryCustomer = customers[2];
                    break;
                case 4:
                    temporaryCustomer = customers[3];
                    break;
                case 5:
                    temporaryCustomer = customers[4];
                    break;
                default:
                    exit = true;
                    break;

            }
            if(exit)
            {
                break;
            }
            System.out.println("Wybierz hurtownie");
            System.out.println(1 + " " + CwholesaleOfBuildingMaterials.mGetWholesaleName + " " + addressesOfWholesales[0].toString());
            System.out.println(2 + " " + CwholesaleOfceramics.mGetWholesaleName + " " + addressesOfWholesales[1].toString());
            System.out.println(3 + " " +  CwholesaleOfHydraulicArticles.mGetWholesaleName + " " + addressesOfWholesales[2].toString());
            number = sc.nextInt();
            switch (number)
            {
                case 1:
                    temporaryWholesale = wholesale1;
                    temporaryWholesale.addCustomer(temporaryCustomer);
                    System.out.print("Właściciel hurtowni: ");
                    getStoreOwner(temporaryWholesale);
                    System.out.println("Wybierz produkt: ");
                    System.out.println(1 + " " + buildingMaterials.getName()+ " cena: " + buildingMaterials.getPrice());
                    System.out.println(2 + " " + looseMaterials.getName()+ " cena: " + looseMaterials.getPrice());
                    number = sc.nextInt();
                    switch(number)
                    {
                        case 1:
                            temporaryProduct = buildingMaterials;
                            System.out.println("Wpisz ilosc: ");
                            number = sc.nextInt();
                            try
                            {
                                temporaryWholesale.getProduct(temporaryProduct, number);
                            }
                            catch(CmyException e)
                            {
                                e.printStackTrace();
                            }
                            temporaryWholesale.removeCustomer();
                            break;
                        case 2:
                            temporaryProduct = looseMaterials;
                            System.out.println("Wpisz ilosc: ");
                            number = sc.nextInt();
                            try
                            {
                                temporaryWholesale.getProduct(temporaryProduct, number);
                            }
                            catch(CmyException e)
                            {
                                e.printStackTrace();
                            }
                            temporaryWholesale.removeCustomer();
                            break;
                        default: exit = true;
                            break;
                    }

                    break;
                case 2:
                    temporaryWholesale = wholesale2;
                    temporaryWholesale.addCustomer(temporaryCustomer);
                    System.out.print("Właściciel hurtowni: ");
                    getStoreOwner(temporaryWholesale);
                    System.out.println("Wybierz produkt: ");
                    System.out.println(1 + " " + ceramics1.getName() + " cena: " + ceramics1.getPrice());
                    System.out.println(2 + " " + glass1.getName() + " cena: " + glass1.getPrice());
                    number = sc.nextInt();
                    switch(number)
                    {
                        case 1:
                            temporaryProduct = ceramics1;
                            System.out.println("Wpisz ilosc: ");
                            number = sc.nextInt();
                            try
                            {
                                temporaryWholesale.getProduct(temporaryProduct, number);
                            }
                            catch(CmyException e)
                            {
                                e.printStackTrace();
                            }
                            temporaryWholesale.removeCustomer();
                            break;
                        case 2:
                            temporaryProduct = glass1;
                            System.out.println("Wpisz ilosc: ");
                            number = sc.nextInt();
                            try
                            {
                                temporaryWholesale.getProduct(temporaryProduct, number);
                            }
                            catch(CmyException e)
                            {
                                e.printStackTrace();
                            }
                            temporaryWholesale.removeCustomer();
                            break;
                        default: exit = true;
                            break;
                    }


                    break;
                case 3:
                    temporaryWholesale = wholesale3;
                    temporaryWholesale.addCustomer(temporaryCustomer);
                    System.out.print("Właściciel hurtowni: ");
                    getStoreOwner(temporaryWholesale);
                    System.out.println("Wybierz produkt: ");
                    System.out.println(1 + " " + hydraulicArticles1.getName() + " cena: " + hydraulicArticles1.getPrice());
                    System.out.println(2 + " " + washbasin.getName() + " cena: " + washbasin.getPrice());
                    number = sc.nextInt();
                    switch(number)
                    {
                        case 1:
                            temporaryProduct = hydraulicArticles1;
                            System.out.println("Wpisz ilosc: ");
                            number = sc.nextInt();
                            try
                            {
                                temporaryWholesale.getProduct(temporaryProduct, number);
                            }
                            catch(CmyException e)
                            {
                                e.printStackTrace();
                            }
                            temporaryWholesale.removeCustomer();
                            break;
                        case 2:
                            temporaryProduct = washbasin;
                            System.out.println("Wpisz ilosc: ");
                            number = sc.nextInt();
                            try
                            {
                                temporaryWholesale.getProduct(temporaryProduct, number);
                            }
                            catch(CmyException e)
                            {
                                e.printStackTrace();
                            }
                            temporaryWholesale.removeCustomer();
                            break;
                        default: exit = true;
                            break;
                    }

                    break;
                default: exit = true;
                    break;
            }
            if(exit)
            {
                break;
            }
            temporaryWholesale.printMapStatus();
            temporaryWholesale.removeCustomer();
            System.out.println("Stan magazynu: ");
            printStatus(temporaryWholesale);
            System.out.println(" ");
            System.out.println("Dziękujemy za zakupy\n");
        }

    }

}
